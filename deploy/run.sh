#!/bin/bash

docker run \
  -it \
  --detach \
  --name confirm \
  --restart always \
  -v /confirm:/confirm \
  -p 5006:5006 \
  bokeh-custom
